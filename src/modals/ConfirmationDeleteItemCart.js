import React, { useState } from 'react';
import {
  View, 
  Modal, 
  StyleSheet, 
  TouchableOpacity, 
  TextInput,
  ToastAndroid
} from 'react-native';

// Import from custom component
import { Text, ButtonComponent } from '../components';

// Get icons from react native vector icons package
import AntDesign from 'react-native-vector-icons/AntDesign'

const ConfirmationItemDelete = ({
  visible,
  closeModal,
  onDelete
}) => {
 
  const onClose = () => {
    setCounter(1)
    setSelectedItems([])
    closeModal()
  }
  

  return (
    <Modal
      transparent={true}
      animationType='fide'
      visible={visible}
      onRequestClose={closeModal}
    >
      <View style={styles.container}>
        <View style={styles.containerBody}>
          <AntDesign name='closecircleo' color='#AA5AFA' size={45} style={{marginBottom: 20}}/>
          <Text>Are you sure to delete item ?</Text>
          <View style={styles.containerButton}>
            <TouchableOpacity style={styles.cancelButton} onPress={closeModal}>
              <Text color='#AA5AFA'>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.confirmButton} onPress={onDelete}>
              <Text>Delete</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container:  {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    flex: 1,
    justifyContent: 'center',
    padding: 54
  },
  containerBody: {
    backgroundColor: '#2D2D2D',
    width: '100%',
    padding: 20,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 30
  },
  containerButton: {
    flexDirection: 'row',
    width: 250,
    justifyContent: 'space-between',
    marginTop: 30
  },
  confirmButton: {
    width: 120,
    height: 40,
    backgroundColor: '#AA5AFA',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelButton: {
    width: 120,
    height: 40,
    borderWidth: 1,
    borderColor: '#AA5AFA',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
})

export default ConfirmationItemDelete;