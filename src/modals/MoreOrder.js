import React, { useState } from 'react';
import {
  View, 
  Modal, 
  StyleSheet, 
  TouchableOpacity, 
  TextInput,
  ToastAndroid
} from 'react-native';

// Import from custom component
import { Text, ButtonComponent } from '../components';

// Get icons from react native vector icons package
import Feather from 'react-native-vector-icons/Feather'

const MoreOrder = ({
  visible,
  closeModal,
  data,
  addOrder
}) => {
  const [counter, setCounter] = useState(1)
  const [selectedItems, setSelectedItems] = useState([])
  const [moreOption, setMoreOption] = useState([
    {
      item: 'Delivery', 
      option: [
        {value: 'JNE', price: 10},
        {value: 'JNT', price: 50},
        {value: 'TIKI', price: 100},
      ]
    },
    {
      item: 'Insurance', 
      option: [
        {value: 'Without insurance', price: 0},
        {value: 'Insurance item', price: 100},
      ]
    },
  ])

  // Function to minus value counter input
  const minCounter = () => {
    if(counter <= 1){
      ToastAndroid.show('value cannot be less than 1', ToastAndroid.SHORT)
    } else {
      setCounter(counter-1)
    }
  }

  // Function to plus value counter input
  const plusCounter = () => {
    setCounter(counter+1)
  }

  // Function for change more option order
  const changeOption = (items) => {
    if(selectedItems.length == 0){
      setSelectedItems([...selectedItems, items])
    } else {
      setSelectedItems(prevItems => {
        const itemIndex = prevItems.findIndex(item => item.item === items.item);
        if (itemIndex > -1) {
          return prevItems.map(item =>
            item.item === items.item ? items : item
          );
        } else {
          return [...prevItems, items];
        }
      });
    }
  }

  // calculate price
  const sumPrices = (arr) => {
    let totalPrice = 0;
    for (let i = 0; i < arr.length; i++) {
      totalPrice += arr[i].price;
    }
   return totalPrice
  };

  const onClose = () => {
    setCounter(1)
    setSelectedItems([])
    closeModal()
  }

  const addSummaryOrderAndTotal = (total) => {
    setSelectedItems([])
    setCounter(1)
    addOrder(total, counter)
  }

  return (
    <Modal
      transparent={true}
      animationType='slide'
      visible={visible}
      onRequestClose={closeModal}
    >
      <View style={styles.container}>
        <View style={styles.containerBody}>
          <TouchableOpacity onPress={onClose} style={styles.buttonLine}/>
          <View style={styles.containerSelectedItems}>
            <Text size={16} type='semiBold'>{data.title}</Text>
            <Text size={16} type='semiBold'>{data.price} NTS</Text>
          </View>
          <View style={styles.containerBodyOption}>
            {moreOption.map(items => (
              <View key={items.item} style={styles.containerOption}>
                <Text type={'semiBold'}>{items.item}</Text>
                {items.option.map(opt => (
                  <TouchableOpacity 
                    key={opt.value} style={styles.containerSubOption}
                    onPress={() => changeOption({item: items.item, price:opt.price})}
                  >
                    <View style={styles.containerRadioButtonOption}>

                      <View style={styles.radioButton}>
                        {selectedItems.map((selected, id) => (
                          selected.item == items.item && selected.price == opt.price && (
                            <View key={id} style={styles.activeButton}/>
                          )
                        ))}
                      </View>
                      <Text>{opt.value}</Text>
                    </View>
                    <Text>{opt.price}</Text>
                  </TouchableOpacity>
                ))}
              </View>
            ))}
            <View style={styles.containerCounter}>
              <TouchableOpacity onPress={minCounter}>
                <Feather name={'minus'} color={'#FFF'} size={22}/>
              </TouchableOpacity>
              <View style={styles.containerInputCounter}>
                <TextInput 
                  style={styles.inputCounter}
                  value={counter.toString()}
                  onChangeText={value => setCounter(value)}
                  maxLength={4}
                  keyboardType='numeric'
                />
              </View>
              <TouchableOpacity onPress={plusCounter}>
                <Feather name={'plus'} color={'#FFF'} size={22}/>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.containerTotal}>
            <Text type='bold'>Total</Text>
            <Text type='bold'>{((counter*data.price)+sumPrices(selectedItems)).toFixed(2)} NTS</Text>
          </View>
          <ButtonComponent 
            title={'Add to cart'}
            onPress={() => addSummaryOrderAndTotal((counter*data.price)+sumPrices(selectedItems))}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container:  {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    flex: 1,
    justifyContent: 'flex-end'
  },
  buttonLine: {
    width: 50,
    height: 6,
    backgroundColor: '#424C5A',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 8,
    marginBottom: 30,
  },
  containerSelectedItems: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  containerBody: {
    backgroundColor: '#1A181D',
    width: '100%',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    padding: 20
  },
  containerBodyOption: {
    backgroundColor: '#221F26',
    width: '100%',
    borderRadius: 8,
    marginTop: 12,
    padding: 12
  },
  containerOption: {
    marginBottom: 16
  },
  containerSubOption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: 8
  },
  containerRadioButtonOption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8,
    alignItems: 'center'
  },
  radioButton: {
    width: 15,
    height: 15,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#BFBEC0',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  activeButton: {
    width: 8,
    height: 8,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#BFBEC0',
    backgroundColor: '#BFBEC0'
  },
  containerCounter: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#898E9A',
    borderRadius: 9,
    backgroundColor: '#16171A',
    height: 50,
    width: '100%',
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerInputCounter: {
    width: 130,
    color: '#FFF',
    fontSize: 30,
    alignItems: 'center',
    padding: 0,
    marginHorizontal: 12
  },
  inputCounter: {
    color: '#FFF',
    fontSize: 30,
    alignItems: 'center',
    padding: 0,
  },
  containerTotal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 18
  },
})

export default MoreOrder;