import React from 'react';
import {
  TextInput, 
  View,
  StyleSheet
} from 'react-native';

// Get for custom component
import {Text} from '../components'

// Get Icon from react native vector icon package
import Father from 'react-native-vector-icons/Feather'

const HeaderNavigation = ({
  title,
  goBack
}) => {
  return (
    <View style={styles.container}>
      <Father name='arrow-left' color={'#FFFFFF'} size={22} onPress={goBack}/>
      <Text color={'#FFF'} type='bold' style={styles.textTitle}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    height: 50,
    alignItems: 'center',
  },
  textTitle: {
   marginLeft: 16
  },
})

export default HeaderNavigation;