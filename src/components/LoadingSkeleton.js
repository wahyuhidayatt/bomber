import React from 'react';
import {View, StyleSheet} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const LoadingSkeleton = ({rows}) => {
  let children = [];
  for (let i = 0; i < rows; i++) {
    children.push(
      <View style={styles.container} key={i}>
        <SkeletonPlaceholder highlightColor='#e5ccfd'>
          <SkeletonPlaceholder.Item flexDirection="row" justifyContent='space-between'>
            <SkeletonPlaceholder.Item backgroundColor={'#000'}>
              <SkeletonPlaceholder.Item
                width={120}
                height={16}
                borderRadius={8}
                marginBottom={8}
              />
              <SkeletonPlaceholder.Item
                width={150}
                height={16}
                borderRadius={8}
                marginBottom={8}
              />
              <SkeletonPlaceholder.Item
                width={70}
                height={16}
                borderRadius={8}
              />
            </SkeletonPlaceholder.Item>
            <SkeletonPlaceholder.Item
              width={90}
              height={90}
              borderRadius={8}
              alignSelf='flex-end'
            />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder>
      </View>,
    );
  }
  return children;
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000000',
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderRadius: 10,
    borderColor: '#F0EEEE',
  },
});

export default LoadingSkeleton;
