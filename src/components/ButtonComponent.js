import React, { useState } from 'react';
import {
  ActivityIndicator, 
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'

// Get for custom component
import {Text} from '../components'

const ButtonComponent = ({
  title,
  loading,
  onPress
}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <LinearGradient
        start={{x: 1, y: 1}} 
        end={{x: 0.4, y: 1}} 
        colors={['#C800CC', '#A060FA']}
        style={{
          width: '90%',
          height: 50,
          flex: 1,
          alignItems: 'center', 
          justifyContent: 'center',
          alignSelf: 'center',
          borderRadius: 8
        }}
      >
        {loading ? 
          <ActivityIndicator size='large' color='#FFF'/> 
          : 
          <Text color={'#FFF'} size={15} type={'semiBold'}>{title}</Text>
        }
      </LinearGradient>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    height: 50,
    alignItems: 'center',
  },
  textTitle: {
   marginLeft: 16
  },
})

export default ButtonComponent;