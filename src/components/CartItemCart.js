import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';

// Get for custom component
import {Text} from '../components'

// Get Icon from react native vector icon package
import AntDesign from 'react-native-vector-icons/AntDesign'

const CardItemCart = ({
  item,
  plusQuantity,
  minQuantity,
  visibleModal
}) => {
  const selected = (params) => {
    visibleModal(params)
  }
  return (
    <View style={styles.container}>
      <View style={styles.containerImgProduct}>
        <Image 
          source={{uri: item.thumbnail}}
          style={styles.imageProduct}
          resizeMode='cover'
        />
      </View>
      <View style={styles.containerDescProduct}>
        <Text type={'semiBold'} style={{marginBottom: 2}}>{item.title}</Text>
        <Text color={'#3CA6EC'} size={12} type={'semiBold'}>NTS {item.price}</Text>
        <AntDesign name='delete' size={20} style={{marginTop: 10}} color='#FFF' onPress={() => selected(item.title)}/>
      </View>
      <View style={styles.containerInputQuantity}>
        <TouchableOpacity style={styles.buttonQuantity} onPress={() => plusQuantity(item.title)}>
          <AntDesign name={'plus'} size={12} color='#FFF'/>
        </TouchableOpacity>
        <Text size={16}>{item.quantity}</Text>
        <TouchableOpacity style={styles.buttonQuantity} onPress={() => minQuantity(item.title)}>
          <AntDesign name={'minus'} size={12} color='#FFF'/>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#343434',
    backgroundColor: '#2D2D2D',
    padding: 14,
    borderRadius: 8,
    marginBottom: 12
  },
  containerDescProduct: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 14
  },
  containerImgProduct: {
    backgroundColor: '#FFF',
    borderRadius: 10,
  },
  imageProduct: {
    width: 104,
    height: 104,
    borderRadius: 8
  },
  containerInputQuantity: {
    backgroundColor: '#000',
    borderRadius: 10,
    padding: 2,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  buttonQuantity: {
    backgroundColor: '#2D2D2D',
    width: 32,
    height: 32,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default CardItemCart;