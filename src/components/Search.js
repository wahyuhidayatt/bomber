import React from 'react';
import {
  TextInput, 
  View,
  StyleSheet
} from 'react-native';

// Get Icon from react native vector icon package
import Father from 'react-native-vector-icons/Feather'

const Search = ({
  placeholder,
  onChangeText, 
  value
}) => {
  return (
    <View style={styles.container}>
      <TextInput
        placeholder={placeholder} 
        onChangeText={(text) => onChangeText(text)}
        value={value}
        placeholderTextColor={'#5D5C5C'}
        style={styles.input}
      />
      <Father name='search' color={'#AA5AFA'} size={22}/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#262626',
    borderWidth: 1,
    borderColor: '#323232',
    borderRadius: 4,
    flexDirection: 'row',
    width: '100%',
    height: 50,
    paddingHorizontal: 12,
    alignItems: 'center'
  },
  input:{
    padding: 0,
    fontFamily: 'Inter-Light',
    color: '#FFFFFF',
    fontSize: 16,
    flex: 1,
    paddingRight: 12
  }
})

export default Search;