import React from 'react';
import {Text, StyleSheet} from 'react-native';

const CustomComponentText = ({
  size,
  fontStyle,
  type,
  color,
  textAlign,
  textDecorationLine,
  opacity,
  letterSpacing,
  style,
  children,
  numberOfLines,
  maxWidth,
}) => {
  const styles = StyleSheet.create({
    Main: {
      fontSize: size ? size : 14,
      fontStyle: fontStyle ? fontStyle : 'normal',
      fontFamily:
        type === 'bold'
          ? 'Inter-Bold'
          : type === 'semiBold'
          ? 'Inter-SemiBold'
          : type === 'black'
          ? 'Inter-Black'
          : type === 'medium'
          ? 'Inter-Medium'
          : type === 'light'
          ? 'Inter-Light'
          : 'Inter-Regular',
      color: color ? color : '#FFF',
      textAlign: textAlign ? textAlign : 'left',
      textDecorationLine: textDecorationLine ? textDecorationLine : 'none',
      opacity: opacity ? opacity : 1,
      letterSpacing: letterSpacing ? letterSpacing : 0,
      maxWidth: maxWidth ? maxWidth : '100%',
    },
  });
  return (
    <Text
      allowFontScaling={false}
      style={[styles.Main, style]}
      numberOfLines={numberOfLines}>
      {children}
    </Text>
  );
};

export default CustomComponentText;
