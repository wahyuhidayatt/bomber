import React from 'react';
import {View, StyleSheet} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const LoadingSkeleton = ({rows}) => {
  let children = [];
  for (let i = 0; i < rows; i++) {
    children.push(
      <View style={styles.container} key={i}>
        <SkeletonPlaceholder highlightColor='#e5ccfd'>
          <SkeletonPlaceholder.Item flexDirection="row" justifyContent='space-between'>
            <SkeletonPlaceholder.Item
              width={120}
              height={16}
              borderRadius={4}
            />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder>
      </View>,
    );
  }
  return children;
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#FFF',
    paddingVertical: 12
  },
});

export default LoadingSkeleton;
