import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';

// Get for custom component
import {Text} from '../components'

const CardItemProduct = ({
  onSelected,
  item
}) => {
  const selected = (params) => {
    onSelected(params)
  }
  return (
    <View style={styles.container}>
      <View style={styles.containerDescProduct}>
        <Text type={'semiBold'} style={{marginBottom: 2}}>{item.title}</Text>
        <Text color={'#3CA6EC'} size={12} type={'semiBold'}>NTS {item.price}</Text>
        <TouchableOpacity style={styles.addButton} onPress={() => selected(item)}>
          <Text>Add item</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.containerImgProduct}>
        <Image 
          source={{uri: item.thumbnail}}
          style={styles.imageProduct}
          resizeMode='cover'
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#343434'
  },
  containerDescProduct: {
    flex: 1,
    justifyContent: 'center'
  },
  containerImgProduct: {
    backgroundColor: '#FFF',
    borderRadius: 10,
  },
  imageProduct: {
    width: 104,
    height: 104,
    borderRadius: 8
  },
  addButton: {
   backgroundColor: '#AB5CFA',
   width: 92,
   height: 31,
   alignItems: 'center',
   justifyContent: 'center',
   borderRadius: 4,
   marginTop: 8
  },
})

export default CardItemProduct;