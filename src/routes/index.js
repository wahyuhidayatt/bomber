// import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import BottomTabNavigator from './BottomTabNavigation';
import Home from '../screens/Home'
import Product from '../screens/Product'
import Cart from '../screens/Cart';
import Payment from '../screens/Payment'
import SplashScreen from '../screens/SplashScreen';

const Stack = createNativeStackNavigator();

const Routes = () => {
  return (
    <NavigationContainer>
       <Stack.Navigator
        initialRouteName="SplashScreen"
        screenOptions={{
          headerShown: false,
          animation: 'slide_from_right',
          gestureEnabled: true
        }}
      >
        <Stack.Screen name="splash" component={SplashScreen} />
        <Stack.Screen name="bottomTabNavigator" component={BottomTabNavigator} />
        <Stack.Screen name="home" component={Home} />
        <Stack.Screen name="product" component={Product} />
        <Stack.Screen name="cart" component={Cart} />
        <Stack.Screen name="payment" component={Payment} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;