import React from 'react';
import { Platform, TouchableOpacity} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

// Import Screen for menu bottom tab
import Home from '../screens/Home';
import Event from '../screens/Event';
import Order from '../screens/Product';
import Friend from '../screens/Event';
import Profile from '../screens/Event';

const Tab = createBottomTabNavigator();

export default MyBsiTabs = ({navigation}) => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        tabBarActiveTintColor: '#FFE419',
        headerShown: false,
        tabBarLabelStyle: {
          marginBottom: Platform.OS == 'ios' ? 0 : 10,
          fontSize: 12,
          fontFamily: 'Catamaran-Regular',
        },
        tabBarStyle: {
          backgroundColor: '#000',
          height: Platform.OS == 'ios' ? 90 : 60,
          borderColor: '#000'
        },
      }}
      >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Nightlife',
          tabBarIcon: ({color, size}) => (
            <Ionicons name="moon-outline" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Event"
        component={Event}
        options={{
          tabBarItemStyle: {
            paddingRight: 0
          },
          tabBarLabel: 'Event',
          tabBarIcon: ({color, size}) => (
            <Feather name="star" color={color} size={24} />
          ),
        }}
      />
       <Tab.Screen
        name="Friend"
        component={Friend}
        options={{
          tabBarItemStyle: {
            paddingLeft: 0
          },
          tabBarLabel: 'Order',
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="groups" color={color} size={24} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="person" color={color} size={24} />
          ),
        }}
      />
      <Tab.Screen
        name="Order"
        component={Order}
        // options={({ navigation }) => ({
        //   tabBarButton: (props) => (
        //     <TouchableOpacity
        //       {...props}
        //       onPress={() => navigation.navigate('Home')}
        //     >
        //       <Text style={{ color: props.color }}>Home</Text>
        //     </TouchableOpacity>
        //   )
        // })}
        options={{
          navigation,
          tabBarItemStyle: {
            paddingLeft: 0
          },
          tabBarLabel: 'Order',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="qrcode" color={color} size={24} />
          ),
          tabBarButton: (props) => (
            <TouchableOpacity
              {...props}
              onPress={() => navigation.navigate('product')}
            />
          )
        }}
      />
    </Tab.Navigator>
  );
};
