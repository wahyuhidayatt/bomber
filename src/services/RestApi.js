import ApiService, {baseUrl} from './index';

export default class RestService {

	GetAllProduct = () => {
		let url = baseUrl + '/product?select=title,price,thumbnail'
		return ApiService.fetchGet(url)
		.then(res => {
			console.log(`[POST] ${url}`, res)
			return res
		})
	}

  GetAllCategoryList = () => {
		let url = baseUrl + '/products/category-list'
		return ApiService.fetchGet(url)
		.then(res => {
			console.log(`[POST] ${url}`, res)
			return res
		})
	}

  GetProductByCategory = (category) => {
		let url = baseUrl + `/products/category/${category}?select=title,price,thumbnail`
		return ApiService.fetchGet(url)
		.then(res => {
			console.log(`[POST] ${url}`, res)
			return res
		})
	}

}