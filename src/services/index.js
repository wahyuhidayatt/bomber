import axios from 'axios';

class ApiService {
  fetchGet = (url) => {
    console.log('request [GET]-', url);
    return axios({
      method: 'GET',
      url: url,
    }).then(response => {
      console.log('response [GET]-', url, response);
      return response;
    });
  };
}

export default new ApiService()
export const baseUrl = 'https://dummyjson.com'

