import React, { useState } from 'react';
import {
  View, 
  StyleSheet, 
  StatusBar, 
  Image,
  ScrollView,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

// Import for custom component 
import {
  Text,
  Search
} from '../components'

// File icon exported from Figma and made assets in the project
const iconBeer = require('../assets/images/icons/ic-beer.png') 
const iconBottle = require('../assets/images/icons/ic-bootle.png') 
const iconLight = require('../assets/images/icons/ic-light.png') 
const iconMic = require('../assets/images/icons/ic-mic.png') 

// File poster event exported from Figma and made assets in the project
const event1 = require('../assets/images/event-dummy-01.png') 
const event2 = require('../assets/images/event-dummy-02.png') 
const event3 = require('../assets/images/event-dummy-03.png') 

const Home = ({navigation}) => {
  // State global 
  const [placeMenu, setPlaceMenu] = useState([
    {img: iconLight, title: 'Nightclub', navigate: ''},
    {img: iconMic, title: 'KTV', navigate: '' },
    {img: iconBeer, title: 'Pregames', navigate: ''},
    {img: iconBottle, title: 'Bar', navigate: ''},
  ])
  const [event, setEvent] = useState([event1, event2, event3])
  const [search, setSearch] = useState('')

  useFocusEffect(
    React.useCallback(() => {
      StatusBar.setBackgroundColor('#000'); // Contoh warna merah
      StatusBar.setBarStyle('light-content');  // Ubah teks status bar menjadi terang
    }, [])
  )

  const onNavigate = (params) => {
    if(params != ''){
      navigation.navigate(params)
    } else {
      ToastAndroid.show('Menu is under development', ToastAndroid.SHORT)
    }
  }
  
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={'#000000'} barStyle={'light-content'}/>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.containerHeader}>
          <Image
            source={require('../assets/images/master-logo.png')} 
            style={styles.headerLogoImage}
            resizeMode='cover'
          />
          <Search
            placeholder={'Search Party'} 
            onChangeText={(text) => setSearch(text)}
          />
        </View>
        <Image
          source={require('../assets/images/ilustration-home.png')} 
          style={styles.bodyImageIlustration}
          resizeMode='cover'
        />
        <Text color='#AA5AFA' type='bold' style={styles.bodyTextMenu}>Find best place</Text>
        <View style={styles.containerMenuPlaces}>
          {placeMenu.map(menu => (
            <TouchableOpacity key={menu.title} style={styles.menuPlaceBox} onPress={() => onNavigate(menu.navigate)}>
              <Image
                source={menu.img} 
                style={styles.menuIconBox}
                resizeMode='center'
              />
              <Text size={10} color={'#FFF'}>{menu.title}</Text>
            </TouchableOpacity>
          ))}
        </View>
        <Text color='#AA5AFA' type='bold' style={styles.bodyTextMenu}>Newest Event</Text>
        <View>
          <ScrollView horizontal={true} contentContainerStyle={styles.containerEvent}>
            {event.map((eventItems, id) => (
              <Image
                key={id}
                source={eventItems} 
                style={styles.eventImage}
                resizeMode='cover'
              />
            ))}
          </ScrollView>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000000',
    flex: 1
  },
  containerHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
    paddingHorizontal: 16
  },
  headerLogoImage: {
    width: 108,
    height: 24,
    marginBottom: 16
  },
  bodyImageIlustration: {
    width: '100%',
    height: 357
  },
  bodyTextMenu:{
    margin: 16
  },
  containerMenuPlaces: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    marginBottom: 16
  },
  menuPlaceBox: {
    width: '23%',
    height: 70,
    alignItems: 'center',
    backgroundColor: 'blue',
    justifyContent:'center',
    backgroundColor: '#262626',
    borderRadius: 8
  },
  menuIconBox: {
    width: 28,
    height: 28,
    marginBottom: 5
  },
  containerEvent: {
    paddingHorizontal: 16
  },
  eventImage: {
    width: 164,
    height: 241,
    marginBottom: 5,
    marginRight: 20
  },
})

export default Home;