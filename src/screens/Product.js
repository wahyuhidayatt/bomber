import React, { useEffect, useState } from 'react';
import {
  View, 
  StyleSheet, 
  StatusBar, 
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';

// Import for custom component 
import {
  HeaderNavigation,
  Search,
  Text,
  CardItemProduct,
  ButtonComponent,
  LoadingSkeleton,
  TabMenuSkeleton
} from '../components'

// Import modal component from custom modal
import {
  MoreOrder
} from '../modals';

// Import service rest API for get data
import ApiService from '../services/RestApi'

const Product = ({navigation}) => {
  // State global 
  const RestApi = new ApiService()
  const [visible, setVisible] = useState(false)
  const [itemSelected, setItemSelected] = useState({})
  const [products, setProducts] = useState([])
  const [tabMenu, setTabMenu] = useState([])
  const [activeMenu, setActiveMenu] = useState('')
  const [refreshing, setRefreshing] = useState(false);
  const [search, setSearch] = useState('');
  const [loadingProduct, setLoadingProduct] = useState(false);
  const [loadingTabMenu, setLoadingTabMenu] = useState(false);
  const [order, setOrder] = useState([]);

  // Get data all product first render
  useEffect(() => {
    getAllProduct()
    getCategoryList()
  },[])

  // Function to refresh page
  const onRefresh = () => {
    setRefreshing(true);
    getAllProduct()
    setActiveMenu('')
  };
  
  // Get all product data
  const getAllProduct = () => {
    setLoadingProduct(true)
    RestApi.GetAllProduct()
    .then(res => {
      setProducts(res.data.products)
    })
    .catch(err => {
      console.log(err)
    })
    .finally(() => {
      setRefreshing(false)
      setTimeout(() => {
        setLoadingProduct(false)
        
      }, 1000);
    })
  }

  // Get all list category
  const getCategoryList = () => {
    setLoadingTabMenu(true)
    RestApi.GetAllCategoryList()
    .then(res => {
      setTabMenu(res.data)
    })
    .catch(err => {
      console.log(err)
    })
    .finally(() => {
      setLoadingTabMenu(false)
    })
  }

  // Get product by category
  const getproductByCategory = (tab) => {
    setLoadingProduct(true)
    RestApi.GetProductByCategory(tab)
    .then(res => {
      setProducts(res.data.products)
    })
    .catch(err => {
      console.log(err)
    })
    .finally(() => {
      setLoadingProduct(false)
    })
  }

  // Function to change status tab menu
  const selectTab = (tab) => {
    setActiveMenu(tab)
    getproductByCategory(tab)
  }

  // Function to selected items to add cart
  const selectedItems = (value) => {
    setItemSelected(value)
    setVisible(!visible)
  }

  // Function add order to cart
  const addOrder = (params, quantity) => {
    const newDataItem = {...itemSelected, total: params, quantity: quantity}
    setOrder(oldArray => [...oldArray, newDataItem])
    setVisible(false)
  }

  // function to calculate total
  const sumPrices = (arr) => {
    let totalPrice = 0;
    for (let i = 0; i < arr.length; i++) {
      totalPrice += arr[i].total;
    }
    return totalPrice
  };

  // Funtion to navigate
  const onNavigate = (order) => {
    if(order.length == 0) {
      ToastAndroid.show('Cart is still empty, please add your items', ToastAndroid.SHORT)
    } else {
      navigation.navigate('cart', {order: order})
    }
  }

  
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={'#000000'} barStyle={'light-content'}/>
      <ScrollView 
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl 
            refreshing={refreshing} 
            onRefresh={onRefresh} 
          />
        }
      >
        <View style={styles.containerHeader}>
          <HeaderNavigation
            title={'Omni Taiwan'} 
            goBack={() => navigation.goBack()}
          />
          <Search
            placeholder={'Search Spirit'} 
            onChangeText={(text) => setSearch(text)}
          />
        </View>
        <View style={styles.containerTabOption}>
          <ScrollView horizontal={true} contentContainerStyle={styles.scrollHorizontalcontainer}>
            {
              loadingTabMenu ? <TabMenuSkeleton rows={5}/> :
              tabMenu.map(tab => (
                <TouchableOpacity key={tab} style={[styles.tabButton, {borderBottomColor: activeMenu == tab ? '#AB5CFA' : '#FFF'}]} onPress={() => selectTab(tab)}>
                  <Text color={activeMenu == tab ? '#AB5CFA' : '#FFF'}>{tab}</Text>
                </TouchableOpacity>
              ))
            }
          </ScrollView>
        </View>
        <View style={styles.containerBody}>
          {
            loadingProduct ? <LoadingSkeleton rows={products.length || 8}/> : 
            products.filter(data => data.title.toLowerCase().includes(search.toLowerCase())).map(product => (
              <CardItemProduct
                key={product.title}
                onSelected={selectedItems}
                item={product}
              />
            ))
          }
          
        </View>
      </ScrollView>
      <ButtonComponent
        title={order.length == 0 ? `View Cart` : `Order : ${order.length} items - total ${(sumPrices(order)).toFixed(2)} NTS`} 
        onPress={() => onNavigate(order)}
      />
      <MoreOrder 
        visible={visible}
        closeModal={() => setVisible(!visible)}
        data={itemSelected}
        addOrder={addOrder}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000000',
    flex: 1
  },
  containerHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16
  },
  containerTabOption: {
    flexDirection: 'row',
    paddingTop: 6
  },
  tabButton: {
    borderBottomWidth: 1,
    paddingVertical: 14,
    paddingHorizontal: 12
  },
  scrollHorizontalcontainer:{
    paddingVertical: 6
  },
  containerBody: {
    paddingHorizontal: 12
  }
})

export default Product;