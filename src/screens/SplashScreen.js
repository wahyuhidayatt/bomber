import React, { useEffect } from 'react';
import {
  View, 
  StyleSheet, 
  StatusBar, 
  Image
} from 'react-native';


const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('bottomTabNavigator')
    }, 1500);
  })
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={'#000'} barStyle={'dark-content'}/>
      <Image
        source={require('../assets/images/master-logo.png')} 
        style={styles.logo}
        resizeMode='cover'
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width:230,
    height: 50
  },
})

export default SplashScreen;