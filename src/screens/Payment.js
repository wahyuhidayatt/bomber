import React, {useState } from 'react';
import {
  View, 
  StyleSheet, 
  StatusBar, 
} from 'react-native';

// Import for custom component 
import {
  Text,
  ButtonComponent
} from '../components'

import AntDesign from 'react-native-vector-icons/AntDesign'

const Payment = ({navigation}) => {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={'#262626'} barStyle={'light-content'}/>
      <AntDesign name='wallet' size={150} color='#AA5AFA'/>
      <Text color='#AA5AFA' type='light' textAlign='center'>{`Currently, direct payments \n are made at the cashier.`}</Text>
      <View style={styles.containerButton}>
        <ButtonComponent 
          title={'Back to home'}
          onPress={() => navigation.navigate('bottomTabNavigator')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#262626',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerButton: {
    marginTop: 30,
    width: '70%'
  },
})

export default Payment;