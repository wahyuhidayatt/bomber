import React, {useState } from 'react';
import {
  View, 
  StyleSheet, 
  StatusBar, 
  ScrollView,
  RefreshControl,
} from 'react-native';

// Import for custom component 
import {
  HeaderNavigation,
  Text,
  CardItemCart,
  ButtonComponent,
  LoadingSkeleton,
} from '../components'

// Import Modal from component modals
import { ConfirmationItemDelete } from '../modals';


const Cart = ({navigation, route}) => {
  // State global 
  const [products, setProducts] = useState([])
  const [refreshing, setRefreshing] = useState(false);
  const [loading, setLoading] = useState(false);
  const [order, setOrder] = useState(route.params.order || [])
  const [visible, setVisible] = useState(false)
  const [itemToDeleted, setDataToDeleted] = useState('')

  // Data from routes params previous screen

  // Function to refresh page
  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(true);
    }, 1000);
  };

  // Function total
  const sumPrices = (arr) => {
    let totalPrice = 0;
    for (let i = 0; i < arr.length; i++) {
      totalPrice += arr[i].total;
    }
    return totalPrice
  };

  // Function to action delete item on cart
  const onDelete = () => {
    let index = order.findIndex(item => item.title === itemToDeleted);
    if (index !== -1) {
      order.splice(index, 1);
    }
    setVisible(!visible)
  }

  const onVisible = (params) => {
    setVisible(!visible)
    setDataToDeleted(params)
  }

  const plusQty = (title) => {
    let newArr = order.map(item => {
      if (item.title === title) {
        return { ...item, quantity: item.quantity+1, total: item.total+item.price}; // Ubah nilai quantity
      }
      return item;
    });
    
    setOrder(newArr)
  }

  const minQty = (title) => {
    let newArr = order.map(item => {
      if (item.title === title) {
        return { ...item, quantity: item.quantity <= 1 ? item.quantity : item.quantity-1, total: item.quantity <= 1 ? item.price : item.total-item.price}; // change quantity value
      }
      return item;
    });
    
    setOrder(newArr)
  }

  const onPayment = () => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
      navigation.navigate('payment')
    }, 1000);
  }


  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={'#262626'} barStyle={'light-content'}/>
        <View style={styles.containerHeader}>
          <HeaderNavigation
            goBack={() => navigation.goBack()}
          />
          <Text size={16} type='semiBold' color='#AA5AFA' style={{marginBottom: 20}}>Cart</Text>
          <View style={styles.containerTotal}>
            <Text>Total</Text>
            <Text type='bold' color='#AA5AFA'>NTS {(sumPrices(order)).toFixed(2)}</Text>
          </View>
        </View>
        <ScrollView 
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl 
              refreshing={refreshing} 
              onRefresh={onRefresh} 
            />
          }
        >
          <View style={styles.containerBody}>
            {
              order?.map((product, id) => (
                <CardItemCart 
                  key={id}
                  item={product}
                  plusQuantity={plusQty}
                  minQuantity={minQty}
                  visibleModal={onVisible}
                />
              ))
            }
          </View>
        </ScrollView>
      <ButtonComponent
        title={order.length == 0 ?'Add items':'Chekout'} 
        onPress={onPayment}
        loading={loading}
      />
      <ConfirmationItemDelete
        visible={visible}
        onDelete={onDelete}
        closeModal={() => setVisible(!visible) }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#262626',
    flex: 1
  },
  containerHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16
  },
  containerBody: {
    paddingHorizontal: 12
  },
  containerTotal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 16
  },
})

export default Cart;