import React, {useState } from 'react';
import {
  View, 
  StyleSheet, 
  StatusBar, 
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

// Import for custom component 
import {
  Text,
} from '../components'

import AntDesign from 'react-native-vector-icons/AntDesign'

const Event = ({navigation}) => {
  useFocusEffect(
    React.useCallback(() => {
      StatusBar.setBackgroundColor('#262626'); // Contoh warna merah
      StatusBar.setBarStyle('light-content');  // Ubah teks status bar menjadi terang
    }, [])
  )
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={'#262626'} barStyle={'light-content'}/>
      <AntDesign name='setting' size={150} color='#AA5AFA' style={{marginBottom: 20}}/>
      <Text color='#AA5AFA' type='light' textAlign='center'>{`Sorry, \n this feature is currently under development.`}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#262626',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerButton: {
    marginTop: 30,
    width: '70%'
  },
})

export default Event;