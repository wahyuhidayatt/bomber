import React from 'react';
// Import routes screen from route navigation file
import Route from './src/routes'

const App = () => {
  return (
    <Route />
  );
};

export default App;